package com.mardus.cycletrack;

import com.mardus.cycletrack.core.providers.base.ProviderDataListener;
import com.mardus.cycletrack.core.providers.base.location.LocationProvider;
import com.mardus.cycletrack.core.providers.base.misc.ServiceUnavailableException;
import com.mardus.cycletrack.core.providers.base.speed.SpeedProvider;
import com.mardus.cycletrack.core.services.CyclingTripService;
import com.mardus.cycletrack.core.services.base.RouteCalculator;

import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;


public class CyclingTripServiceTest {

    @Test(expected = ServiceUnavailableException.class)
    public void testServiceUnavailable() throws Exception {
        RouteCalculator routeCalculator = Mockito.mock(RouteCalculator.class);

        LocationProvider locationProvider = Mockito.mock(LocationProvider.class);
        Mockito.doThrow(new ServiceUnavailableException()).when(locationProvider).startTracking();

        SpeedProvider speedProvider = Mockito.mock(SpeedProvider.class);

        CyclingTripService tripService = new CyclingTripService(speedProvider, locationProvider, routeCalculator);
        tripService.startTrip();
    }

    @Test
    public void testStartService() throws Exception {
        RouteCalculator routeCalculator = Mockito.mock(RouteCalculator.class);

        LocationProvider locationProvider = Mockito.mock(LocationProvider.class);
        SpeedProvider speedProvider = Mockito.mock(SpeedProvider.class);

        CyclingTripService tripService = new CyclingTripService(speedProvider, locationProvider, routeCalculator);
        tripService.startTrip();

        verify(locationProvider, atLeastOnce()).startTracking();
    }

    @Test
    public void textStopService() throws Exception {
        RouteCalculator routeCalculator = Mockito.mock(RouteCalculator.class);

        LocationProvider locationProvider = Mockito.mock(LocationProvider.class);
        SpeedProvider speedProvider = Mockito.mock(SpeedProvider.class);

        CyclingTripService tripService = new CyclingTripService(speedProvider, locationProvider, routeCalculator);
        tripService.startTrip();
        tripService.stopTrip();

        verify(locationProvider, atLeastOnce()).removeDataListener(any(ProviderDataListener.class));
        verify(speedProvider, atLeastOnce()).removeDataListener(any(ProviderDataListener.class));
        verify(speedProvider, atLeastOnce()).stopTracking();
    }
}