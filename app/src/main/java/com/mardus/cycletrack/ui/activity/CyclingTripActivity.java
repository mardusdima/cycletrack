package com.mardus.cycletrack.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.mardus.cycletrack.App;
import com.mardus.cycletrack.core.CyclingManager;
import com.mardus.cycletrack.R;
import com.mardus.cycletrack.core.providers.base.misc.ServiceNotConnectedException;
import com.mardus.cycletrack.core.providers.base.misc.ServiceUnavailableException;
import com.mardus.cycletrack.core.services.base.TripService;
import com.mardus.cycletrack.ui.activity.base.BaseCyclingActivity;
import com.mardus.cycletrack.ui.adapters.CyclingSectionsPagerAdapter;
import com.mardus.cycletrack.ui.service.CyclingTripAndroidService;
import com.mardus.cycletrack.utils.Utils;

public class CyclingTripActivity extends BaseCyclingActivity implements NavigationView.OnNavigationItemSelectedListener, CyclingManager.SettingsChangeListener {

    private TripService tripService;

    private FloatingActionButton newTrackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_track);

        tripService = cyclingManager.getTripService();

        initViewPager();
        initListeners();
        Toolbar toolbar = initToolbar();
        initDrawer(toolbar);

        Utils.requestPermissionsIfNeeded(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        cyclingManager.addSettingsChangeListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        cyclingManager.removeSettingsChangeListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        item.setChecked(true);
        stopTrip();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        int itemId = item.getItemId();
        if (itemId == R.id.menu_google_api_sensors) {
            cyclingManager.initGoogleProviders();
        } else if (itemId == R.id.menu_google_fit_sensors) {
            cyclingManager.initFitProviders();
        } else if (itemId == R.id.menu_mocked_sensors) {
            cyclingManager.initMockedProviders();
        }

        return true;
    }

    private Toolbar initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        return toolbar;
    }

    private void initListeners() {
        newTrackButton = (FloatingActionButton) findViewById(R.id.fab);
        updateStartTripButton();
        newTrackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tripService.isTripStarted()) {
                    stopTrip();
                } else {
                    startTrip();
                }
            }
        });
    }

    private void initDrawer(Toolbar toolbar) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.app_name, R.string.app_name);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        switch (cyclingManager.getProviderType()) {
            case 0:
                navigationView.setCheckedItem(R.id.menu_google_api_sensors);
                break;
            case 1:
                navigationView.setCheckedItem(R.id.menu_google_fit_sensors);
                break;
            case 2:
                navigationView.setCheckedItem(R.id.menu_mocked_sensors);
                break;
        }
    }

    private void initViewPager() {
        CyclingSectionsPagerAdapter sectionsPagerAdapter = new CyclingSectionsPagerAdapter(getSupportFragmentManager());

        ViewPager viewPager = (ViewPager) findViewById(R.id.container);
        viewPager.setAdapter(sectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void startTrip() {
        try {
            tripService.startTrip();
            startService(new Intent(CyclingTripActivity.this, CyclingTripAndroidService.class));
            updateStartTripButton();
        } catch (ServiceUnavailableException | ServiceNotConnectedException e) {
            Utils.requestPermissionsIfNeeded(this);
        }
    }

    private void stopTrip() {
        tripService.stopTrip();
        stopService(new Intent(CyclingTripActivity.this, CyclingTripAndroidService.class));
        updateStartTripButton();
    }

    private void updateStartTripButton() {
        newTrackButton.setImageResource(tripService.isTripStarted() ? android.R.drawable.ic_media_pause : android.R.drawable.ic_media_play);
    }

    @Override
    public void onSettingChanged() {
        tripService = cyclingManager.getTripService();
        initViewPager();
    }
}
