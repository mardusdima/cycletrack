package com.mardus.cycletrack.ui.fragment.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.mardus.cycletrack.core.CyclingManager;
import com.mardus.cycletrack.ui.activity.CyclingTripActivity;

public class BaseFragment extends Fragment {

    protected CyclingManager cyclingManager;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FragmentActivity activity = getActivity();
        if (activity instanceof CyclingTripActivity) {
            cyclingManager = ((CyclingTripActivity) activity).getCyclingManager();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        cyclingManager = null;
    }
}
