package com.mardus.cycletrack.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mardus.cycletrack.R;
import com.mardus.cycletrack.utils.Utils;

import java.util.Locale;

public class CyclingInfoFragment extends CyclingTripFragment {
    private static final String SPEED_FORMAT = "%.1f";
    private static final String DISTANCE_FORMAT = "%.1f";

    private TextView timeTextView;
    private TextView speedTextView;
    private TextView distanceTextView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cycling_info, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        timeTextView = (TextView) view.findViewById(R.id.timeTextView);
        speedTextView = (TextView) view.findViewById(R.id.speedTextView);
        distanceTextView = (TextView) view.findViewById(R.id.distanceTextView);
        updateUIValues(0, 0, 0);
    }

    @Override
    public void onTripInfoChanged(final float speed, final int distance, final long time) {
        if (!isAdded()) {
            return;
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                updateUIValues(time, speed, distance);
            }
        });
    }

    private void updateUIValues(long seconds, float speed, int distance) {
        long milliseconds = seconds * 1000;
        timeTextView.setText(Utils.formatInterval(milliseconds));
        float kmHour = speed * 3.6f;
        speedTextView.setText(getString(R.string.speed_pattern, String.format(Locale.getDefault(), SPEED_FORMAT, kmHour)));
        float km = distance / 1000f;
        distanceTextView.setText(getString(R.string.distance_pattern, String.format(Locale.getDefault(), DISTANCE_FORMAT, km)));
    }
}
