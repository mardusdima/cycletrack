package com.mardus.cycletrack.ui.fragment;

import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.mardus.cycletrack.R;
import com.mardus.cycletrack.core.providers.base.ProviderConnectionListener;
import com.mardus.cycletrack.core.providers.base.ProviderData;
import com.mardus.cycletrack.core.providers.base.ProviderDataListener;
import com.mardus.cycletrack.core.providers.base.location.LocationProvider;
import com.mardus.cycletrack.core.providers.base.misc.ServiceNotConnectedException;
import com.mardus.cycletrack.core.providers.base.misc.ServiceUnavailableException;
import com.mardus.cycletrack.model.GeoPoint;
import com.mardus.cycletrack.model.Route;
import com.mardus.cycletrack.utils.Utils;

import java.util.Date;
import java.util.List;

public class CyclingMapFragment extends CyclingTripFragment implements OnMapReadyCallback, ProviderConnectionListener {

    public static final int MAP_ZOOM = 17;

    private SupportMapFragment mapFragment;

    private LocationProvider locationProvider;
    private GoogleMap googleMap;
    private boolean shouldDrawRoute;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cycling_map, null);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        locationProvider = cyclingManager.getLocationProvider();
        try {
            locationProvider.connect(this);
        } catch (ServiceUnavailableException e) {
            showUIErrorToast();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (!isAdded()) {
            return;
        }
        this.googleMap = googleMap;

        if (!Utils.isLocationPermissionsEnabled(getActivity())) {
            return;
        }
        googleMap.setLocationSource(new MyLocationSource());
        try {
            googleMap.setMyLocationEnabled(true);
        } catch (SecurityException e) {
            //no need
        }
    }

    @Override
    public void onProviderConnected() {
        try {
            locationProvider.startTracking();
        } catch (ServiceUnavailableException | ServiceNotConnectedException e) {
            showUIErrorToast();
            //TODO process exception
        }
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onProviderConnectionError() {
        showUIErrorToast();
        //TODO process error
    }

    @Override
    public void onTripStarted() {
        shouldDrawRoute = true;
        if (googleMap != null) {
            googleMap.clear();
        }
    }

    @Override
    public void onTripStopped() {
        shouldDrawRoute = false;
    }

    private void showUIErrorToast() {
        Toast.makeText(getActivity(), R.string.connection_error, Toast.LENGTH_SHORT).show();
    }

    private void updateGoogleMap(Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, MAP_ZOOM);
        googleMap.animateCamera(cameraUpdate);

        Route route = tripService.getRoute();
        if (!shouldDrawRoute || route == null) {
            return;
        }
        googleMap.clear();
        PolylineOptions options = new PolylineOptions().width(15).color(Color.parseColor("#A012B7D5")).geodesic(true);
        List<Pair<Date, GeoPoint>> geoPointList = route.getGeoPointList();
        for (Pair<Date, GeoPoint> pathElement : geoPointList) {
            LatLng point = new LatLng(pathElement.second.latitude, pathElement.second.longitude);
            options.add(point);
        }
        googleMap.addPolyline(options);
    }

    private class MyLocationSource implements LocationSource, ProviderDataListener<GeoPoint> {
        private static final String MOCK = "Mock";

        private OnLocationChangedListener onLocationChangedListener;

        @Override
        public void activate(final OnLocationChangedListener onLocationChangedListener) {
            this.onLocationChangedListener = onLocationChangedListener;
            locationProvider.addDataListener(this);
            try {
                locationProvider.startTracking();
            } catch (ServiceUnavailableException | ServiceNotConnectedException e) {
                showUIErrorToast();
            }
        }

        @Override
        public void deactivate() {
            locationProvider.removeDataListener(this);
            locationProvider.stopTracking();
        }

        @Override
        public void onDataChanged(ProviderData<GeoPoint> data) {
            GeoPoint geoPoint = data.getValue();

            final Location location = new Location(MOCK);
            location.setLatitude(geoPoint.latitude);
            location.setLongitude(geoPoint.longitude);

            onLocationChangedListener.onLocationChanged(location);
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateGoogleMap(location);
                }
            });
        }
    }
}
