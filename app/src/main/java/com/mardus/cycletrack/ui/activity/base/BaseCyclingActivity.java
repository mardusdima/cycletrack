package com.mardus.cycletrack.ui.activity.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.mardus.cycletrack.App;
import com.mardus.cycletrack.core.CyclingManager;

public class BaseCyclingActivity extends AppCompatActivity {

    protected CyclingManager cyclingManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (App.getInstance().getCyclingManager() == null) {
            App.getInstance().initCyclingManager(this);
        }
        cyclingManager = App.getInstance().getCyclingManager();
    }

    public CyclingManager getCyclingManager() {
        return cyclingManager;
    }

}
