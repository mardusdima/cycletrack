package com.mardus.cycletrack.ui.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.mardus.cycletrack.App;
import com.mardus.cycletrack.R;
import com.mardus.cycletrack.core.services.base.TripListener;
import com.mardus.cycletrack.core.services.base.TripService;
import com.mardus.cycletrack.model.GeoPoint;
import com.mardus.cycletrack.ui.activity.CyclingTripActivity;
import com.mardus.cycletrack.utils.Utils;

import java.util.Locale;

public class CyclingTripAndroidService extends Service implements TripListener {
    private static final int NOTIFICATION_ID = 1;
    private static final String DISTANCE_FORMAT = "%.1f";

    private TripService tripService;

    @Override
    public void onCreate() {
        super.onCreate();
        tripService = App.getInstance().getCyclingManager().getTripService();
        tripService.addTripListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        tripService.removeTripListener(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Notification notification = getNotification(0, 0);
        startForeground(NOTIFICATION_ID, notification);

        return START_STICKY;
    }

    private Notification getNotification(int distance, long time) {
        String formattedDistance = String.format(Locale.getDefault(), DISTANCE_FORMAT, distance / 1000f);
        String formattedTime = Utils.formatInterval(time * 1000);
        Intent notificationIntent = new Intent(this, CyclingTripActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        return new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.app_is_running))
                .setContentText(getString(R.string.notification_pattern, formattedTime, formattedDistance))
                .setContentIntent(pendingIntent).build();
    }

    @Override
    public void onTripInfoChanged(float speed, int distance, long time) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = getNotification(distance, time);
        notificationManager.notify(NOTIFICATION_ID, notification);
    }

    @Override
    public void onLocationChanged(GeoPoint location) {
        //no need
    }

    @Override
    public void onTripStarted() {
        //no need
    }

    @Override
    public void onTripStopped() {
        //no need
    }
}
