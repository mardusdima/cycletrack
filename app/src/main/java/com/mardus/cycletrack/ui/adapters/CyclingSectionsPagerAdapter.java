package com.mardus.cycletrack.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.mardus.cycletrack.App;
import com.mardus.cycletrack.R;
import com.mardus.cycletrack.ui.fragment.CyclingMapFragment;
import com.mardus.cycletrack.ui.fragment.CyclingInfoFragment;

public class CyclingSectionsPagerAdapter extends FragmentStatePagerAdapter {
    private static final int PAGE_COUNT = 2;
    private static final int PAGE_CYCLING_INFO = 0;
    private static final int PAGE_CYCLING_MAP = 1;

    public CyclingSectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case PAGE_CYCLING_INFO:
                return new CyclingInfoFragment();
            case PAGE_CYCLING_MAP:
                return new CyclingMapFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case PAGE_CYCLING_INFO:
                return App.getInstance().getString(R.string.information);
            case PAGE_CYCLING_MAP:
                return App.getInstance().getString(R.string.trip_map);
        }
        return null;
    }
}
