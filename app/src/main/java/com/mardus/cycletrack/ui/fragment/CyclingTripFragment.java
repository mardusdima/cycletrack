package com.mardus.cycletrack.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.mardus.cycletrack.core.services.base.TripListener;
import com.mardus.cycletrack.core.services.base.TripService;
import com.mardus.cycletrack.model.GeoPoint;
import com.mardus.cycletrack.ui.fragment.base.BaseFragment;

public abstract class CyclingTripFragment extends BaseFragment implements TripListener {
    protected TripService tripService;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        tripService = cyclingManager.getTripService();
    }

    @Override
    public void onResume() {
        super.onResume();
        tripService.addTripListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        tripService.removeTripListener(this);
    }

    @Override
    public void onTripInfoChanged(float speed, int distance, long time) {
        //stub
    }

    @Override
    public void onLocationChanged(GeoPoint location) {
        //stub
    }

    @Override
    public void onTripStarted() {
        //stub
    }

    @Override
    public void onTripStopped() {
        //stub
    }
}
