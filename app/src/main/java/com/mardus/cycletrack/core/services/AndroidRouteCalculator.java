package com.mardus.cycletrack.core.services;

import android.location.Location;
import android.support.v4.util.Pair;

import com.mardus.cycletrack.core.services.base.RouteCalculator;
import com.mardus.cycletrack.model.GeoPoint;
import com.mardus.cycletrack.model.Route;

import java.util.Date;
import java.util.List;

public class AndroidRouteCalculator implements RouteCalculator {

    @Override
    public int calculateDistance(Route route) {
        int distance = 0;
        List<Pair<Date, GeoPoint>> geoPointList = route.getGeoPointList();
        if (geoPointList.size() < 2) {
            return 0;
        }
        for (int i = 0; i < geoPointList.size() - 2; i++) {
            GeoPoint firstGeoPoint = geoPointList.get(i).second;
            Location firstLocation = new Location("");
            firstLocation.setLongitude(firstGeoPoint.longitude);
            firstLocation.setLatitude(firstGeoPoint.longitude);

            GeoPoint secondGeoPoint = geoPointList.get(i + 1).second;
            Location secondLocation = new Location("");
            secondLocation.setLongitude(secondGeoPoint.longitude);
            secondLocation.setLatitude(secondGeoPoint.longitude);

            distance += firstLocation.distanceTo(secondLocation);
        }
        return distance;
    }
}
