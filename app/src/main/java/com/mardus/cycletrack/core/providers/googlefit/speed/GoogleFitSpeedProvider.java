package com.mardus.cycletrack.core.providers.googlefit.speed;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.Value;
import com.google.android.gms.fitness.request.DataSourcesRequest;
import com.google.android.gms.fitness.result.DataSourcesResult;
import com.mardus.cycletrack.core.providers.base.speed.SpeedProvider;
import com.mardus.cycletrack.core.providers.googlefit.GoogleFitProvider;

public class GoogleFitSpeedProvider extends GoogleFitProvider<Float> implements SpeedProvider {

    public GoogleFitSpeedProvider(GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    @Override
    protected DataSourcesRequest getDataSources() {
        return new DataSourcesRequest.Builder()
                .setDataTypes(DataType.TYPE_SPEED)
                .setDataSourceTypes(DataSource.TYPE_DERIVED)
                .build();
    }

    @Override
    protected void processDataSourcesResult(DataSourcesResult dataSourcesResult) {
        for (DataSource dataSource : dataSourcesResult.getDataSources()) {
            if (dataSource.getDataType().equals(DataType.TYPE_SPEED)) {
                registerFitnessDataListener(dataSource, DataType.TYPE_SPEED);
            }
        }
    }

    @Override
    public void onDataPoint(DataPoint dataPoint) {
        float speed = 0;
        for (Field field : dataPoint.getDataType().getFields()) {
            Value value = dataPoint.getValue(field);
            if (field.equals(Field.FIELD_SPEED)) {
                speed = value.asFloat();
            }
        }
        sendDataToListeners(speed);
    }
}
