package com.mardus.cycletrack.core.providers.googlefit;

import com.google.android.gms.common.api.GoogleApiClient;
import com.mardus.cycletrack.core.providers.base.ProvidersFactory;
import com.mardus.cycletrack.core.providers.base.location.LocationProvider;
import com.mardus.cycletrack.core.providers.base.speed.SpeedProvider;
import com.mardus.cycletrack.core.providers.googlefit.location.GoogleFitLocationProvider;
import com.mardus.cycletrack.core.providers.googlefit.speed.GoogleFitSpeedProvider;

public class GoogleFitProvidersFactory implements ProvidersFactory {
    private GoogleApiClient googleApiClient;

    public GoogleFitProvidersFactory(GoogleApiClient googleApiClient) {
        this.googleApiClient = googleApiClient;
    }

    @Override
    public LocationProvider getLocationProvider() {
        return new GoogleFitLocationProvider(googleApiClient);
    }

    @Override
    public SpeedProvider getSpeedProvider() {
        return new GoogleFitSpeedProvider(googleApiClient);
    }
}
