package com.mardus.cycletrack.core.providers.mock;

import com.mardus.cycletrack.core.providers.base.ProviderConnectionListener;
import com.mardus.cycletrack.core.providers.base.SimpleProvider;
import com.mardus.cycletrack.core.providers.base.misc.ServiceNotConnectedException;
import com.mardus.cycletrack.core.providers.base.misc.ServiceUnavailableException;

import java.util.Timer;
import java.util.TimerTask;

public abstract class SimpleMockedProvider<T> extends SimpleProvider<T> {
    private static final int DELAY = 1000;

    private Timer timer;
    private boolean isRunning;

    @Override
    public boolean isAvailable() {
        return true;
    }

    @Override
    public boolean isConnected() throws ServiceUnavailableException {
        return true;
    }

    @Override
    public void connect(ProviderConnectionListener providerConnectionListener) throws ServiceUnavailableException {
        providerConnectionListener.onProviderConnected();
    }

    @Override
    public void startTracking() throws ServiceUnavailableException, ServiceNotConnectedException {
        if (isRunning) {
            return;
        }
        isRunning = true;
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                sendMockedData();
            }
        }, getDelay(), getDelay());
    }

    @Override
    public void stopTracking() {
        if (!isRunning) {
            return;
        }
        isRunning = false;
        timer.cancel();
    }

    protected abstract void sendMockedData();

    protected int getDelay() {
        return DELAY;
    }
}
