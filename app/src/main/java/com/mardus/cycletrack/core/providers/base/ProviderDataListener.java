package com.mardus.cycletrack.core.providers.base;

public interface ProviderDataListener<T> {

    void onDataChanged(ProviderData<T> data);
}
