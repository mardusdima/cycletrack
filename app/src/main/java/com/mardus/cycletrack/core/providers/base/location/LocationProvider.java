package com.mardus.cycletrack.core.providers.base.location;

import com.mardus.cycletrack.core.providers.base.BaseProvider;
import com.mardus.cycletrack.model.GeoPoint;

public interface LocationProvider extends BaseProvider<GeoPoint> {

}
