package com.mardus.cycletrack.core.providers.base.fitness;

import com.mardus.cycletrack.core.providers.base.BaseProvider;

public interface CaloriesProvider extends BaseProvider<Integer> {
}
