package com.mardus.cycletrack.core.providers.googlefit;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.request.DataSourcesRequest;
import com.google.android.gms.fitness.request.OnDataPointListener;
import com.google.android.gms.fitness.request.SensorRequest;
import com.google.android.gms.fitness.result.DataSourcesResult;
import com.mardus.cycletrack.core.providers.base.ProviderConnectionListener;
import com.mardus.cycletrack.core.providers.base.SimpleProvider;
import com.mardus.cycletrack.core.providers.base.misc.ServiceNotConnectedException;
import com.mardus.cycletrack.core.providers.base.misc.ServiceUnavailableException;

import java.util.concurrent.TimeUnit;

public abstract class GoogleFitProvider<T> extends SimpleProvider<T> implements OnDataPointListener, GoogleApiClient.ConnectionCallbacks {
    private GoogleApiClient googleApiClient;

    public GoogleFitProvider(GoogleApiClient googleApiClient) {
        this.googleApiClient = googleApiClient;
    }

    @Override
    public boolean isAvailable() {
        return true;
    }

    @Override
    public boolean isConnected() throws ServiceUnavailableException {
        return googleApiClient.isConnected();
    }

    @Override
    public void connect(ProviderConnectionListener providerConnectionListener) throws ServiceUnavailableException {
        super.connect(providerConnectionListener);
        googleApiClient.registerConnectionCallbacks(this);
        googleApiClient.registerConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
            @Override
            public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                GoogleFitProvider.this.providerConnectionListener.onProviderConnectionError();
            }
        });
        googleApiClient.connect();
    }

    @Override
    public void startTracking() throws ServiceUnavailableException, ServiceNotConnectedException {
        findFitnessDataSources();
    }

    @Override
    public void stopTracking() {
        unregisterFitnessDataListener();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        GoogleFitProvider.this.providerConnectionListener.onProviderConnected();
        findFitnessDataSources();
    }

    @Override
    public void onConnectionSuspended(int i) {
        //no need
    }

    protected abstract DataSourcesRequest getDataSources();

    protected abstract void processDataSourcesResult(DataSourcesResult dataSourcesResult);

    private void findFitnessDataSources() {
        Fitness.SensorsApi.findDataSources(googleApiClient, getDataSources())
                .setResultCallback(new ResultCallback<DataSourcesResult>() {
                    @Override
                    public void onResult(@NonNull DataSourcesResult dataSourcesResult) {
                        processDataSourcesResult(dataSourcesResult);
                    }
                });
    }

    protected void registerFitnessDataListener(DataSource dataSource, DataType dataType) {
        SensorRequest sensorRequest = new SensorRequest.Builder()
                .setDataSource(dataSource) // Optional but recommended for custom data sets.
                .setDataType(dataType) // Can't be omitted.
                .setSamplingRate(3, TimeUnit.SECONDS)
                .build();
        Fitness.SensorsApi.add(googleApiClient, sensorRequest, this);
    }

    private void unregisterFitnessDataListener() {
        if (googleApiClient.isConnected()) {
            Fitness.SensorsApi.remove(googleApiClient, this);
        }
    }
}
