package com.mardus.cycletrack.core.providers.mock.location;

import com.mardus.cycletrack.core.providers.base.location.LocationProvider;
import com.mardus.cycletrack.core.providers.mock.SimpleMockedProvider;
import com.mardus.cycletrack.model.GeoPoint;

public class LocationMockedProvider extends SimpleMockedProvider<GeoPoint> implements LocationProvider {
    private static final int DELAY = 3000;

    private static final GeoPoint[] LONDON_CIRCLE_ROUTE = new GeoPoint[]{
            new GeoPoint(51.5176265, -0.1213779),
            new GeoPoint(51.517727, -0.1204873),
            new GeoPoint(51.517787, -0.1193393),
            new GeoPoint(51.517864, -0.1184593),
            new GeoPoint(51.517991, -0.1175583),
            new GeoPoint(51.518058, -0.1169143),
            new GeoPoint(51.517611, -0.1167853),
            new GeoPoint(51.517414, -0.1175903),
            new GeoPoint(51.517267, -0.1183253),
            new GeoPoint(51.516997, -0.1192753),
            new GeoPoint(51.516944, -0.1199293),
            new GeoPoint(51.517201, -0.1205893),
            new GeoPoint(51.517164, -0.1211583),
            new GeoPoint(51.517611, -0.1214103)
    };

    private int circlePointPosition = 0;

    @Override
    protected void sendMockedData() {
        circlePointPosition = circlePointPosition % LONDON_CIRCLE_ROUTE.length;
        sendDataToListeners(LONDON_CIRCLE_ROUTE[circlePointPosition++]);
    }

    @Override
    protected int getDelay() {
        return DELAY;
    }
}