package com.mardus.cycletrack.core.services.base;

import com.mardus.cycletrack.model.Route;

public interface RouteCalculator {

    int calculateDistance(Route route);
}
