package com.mardus.cycletrack.core.providers.base;

public interface ProviderConnectionListener {

    void onProviderConnected();

    void onProviderConnectionError();
}
