package com.mardus.cycletrack.core.providers.base.speed;

import com.mardus.cycletrack.core.providers.base.BaseProvider;

public interface SpeedProvider extends BaseProvider<Float> {

}
