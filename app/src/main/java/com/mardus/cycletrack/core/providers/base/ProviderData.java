package com.mardus.cycletrack.core.providers.base;

public interface ProviderData<T> {

    T getValue();
}
