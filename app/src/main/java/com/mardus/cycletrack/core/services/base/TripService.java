package com.mardus.cycletrack.core.services.base;

import com.mardus.cycletrack.core.providers.base.misc.ServiceNotConnectedException;
import com.mardus.cycletrack.core.providers.base.misc.ServiceUnavailableException;
import com.mardus.cycletrack.model.Route;

public interface TripService {

    void addTripListener(TripListener tripListener);

    void removeTripListener(TripListener tripListener);

    boolean isTripStarted();

    void startTrip() throws ServiceUnavailableException, ServiceNotConnectedException;

    void stopTrip();

    Route getRoute();
}
