package com.mardus.cycletrack.core;

import com.mardus.cycletrack.core.providers.base.ProvidersFactory;
import com.mardus.cycletrack.core.providers.base.location.LocationProvider;
import com.mardus.cycletrack.core.providers.base.speed.SpeedProvider;
import com.mardus.cycletrack.core.services.CyclingTripService;
import com.mardus.cycletrack.core.services.base.RouteCalculator;
import com.mardus.cycletrack.core.services.base.TripService;

import java.util.ArrayList;
import java.util.List;

public class CyclingManager {
    private List<SettingsChangeListener> settingsChangeListeners;

    private ProvidersFactory googleProvidersFactory;
    private ProvidersFactory fitnessProvidersFactory;
    private ProvidersFactory mockedProvidersFactory;
    private RouteCalculator routeCalculator;

    private LocationProvider locationProvider;
    private SpeedProvider speedProvider;
    private TripService tripService;

    private int providerType;

    public CyclingManager(ProvidersFactory googleProvidersFactory, ProvidersFactory fitnessProvidersFactory, ProvidersFactory mockedProvidersFactory, RouteCalculator routeCalculator) {
        this.googleProvidersFactory = googleProvidersFactory;
        this.fitnessProvidersFactory = fitnessProvidersFactory;
        this.mockedProvidersFactory = mockedProvidersFactory;
        this.routeCalculator = routeCalculator;
        this.settingsChangeListeners = new ArrayList<>();
    }

    public TripService getTripService() {
        return tripService;
    }

    public int getProviderType() {
        return providerType;
    }

    public void initGoogleProviders() {
        stopProviders();
        providerType = 0;
        initTripService(googleProvidersFactory);
    }

    public void initFitProviders() {
        stopProviders();
        providerType = 1;
        initTripService(fitnessProvidersFactory);
    }

    public void initMockedProviders() {
        stopProviders();
        providerType = 2;
        initTripService(mockedProvidersFactory);
    }

    public void addSettingsChangeListener(SettingsChangeListener settingsChangeListener) {
        settingsChangeListeners.add(settingsChangeListener);
    }

    public void removeSettingsChangeListener(SettingsChangeListener settingsChangeListener) {
        settingsChangeListeners.remove(settingsChangeListener);
    }

    private void initTripService(ProvidersFactory providersFactory) {
        if (tripService != null) {
            tripService.stopTrip();
            tripService = null;
        }
        this.locationProvider = providersFactory.getLocationProvider();
        this.speedProvider = providersFactory.getSpeedProvider();
        this.tripService = new CyclingTripService(speedProvider, locationProvider, routeCalculator);

        for (SettingsChangeListener settingsChangeListener : settingsChangeListeners) {
            settingsChangeListener.onSettingChanged();
        }
    }

    private void stopProviders() {
        if (tripService != null) {
            tripService.stopTrip();
        }
        if (locationProvider != null) {
            locationProvider.stopTracking();
        }
        if (speedProvider != null) {
            speedProvider.stopTracking();
        }
    }

    public LocationProvider getLocationProvider() {
        return locationProvider;
    }

    public interface SettingsChangeListener {

        void onSettingChanged();
    }
}
