package com.mardus.cycletrack.core.providers.google.location;

import android.location.Location;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.mardus.cycletrack.core.providers.base.location.LocationProvider;
import com.mardus.cycletrack.core.providers.google.GoogleServicesLocationBasedProvider;
import com.mardus.cycletrack.model.GeoPoint;

public class GoogleServicesLocationProvider extends GoogleServicesLocationBasedProvider<GeoPoint> implements LocationProvider, LocationListener {

    public GoogleServicesLocationProvider(GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    @Override
    protected GeoPoint extractDataFromLocation(Location location) {
        return new GeoPoint(location.getLatitude(), location.getLongitude());
    }

}
