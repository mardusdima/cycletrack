package com.mardus.cycletrack.core.services.base;

import com.mardus.cycletrack.model.GeoPoint;

public interface TripListener {

    void onTripInfoChanged(float speed, int distance, long time);

    void onLocationChanged(GeoPoint location);

    void onTripStarted();

    void onTripStopped();
}
