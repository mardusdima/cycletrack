package com.mardus.cycletrack.core.providers.mock;

import com.mardus.cycletrack.core.providers.base.ProvidersFactory;
import com.mardus.cycletrack.core.providers.base.location.LocationProvider;
import com.mardus.cycletrack.core.providers.base.speed.SpeedProvider;
import com.mardus.cycletrack.core.providers.mock.location.LocationMockedProvider;
import com.mardus.cycletrack.core.providers.mock.speed.SpeedMockedProvider;

public class MockedServicesFactory implements ProvidersFactory {
    @Override
    public LocationProvider getLocationProvider() {
        return new LocationMockedProvider();
    }

    @Override
    public SpeedProvider getSpeedProvider() {
        return new SpeedMockedProvider();
    }
}
