package com.mardus.cycletrack.core.providers.google;

import com.google.android.gms.common.api.GoogleApiClient;
import com.mardus.cycletrack.core.providers.base.ProvidersFactory;
import com.mardus.cycletrack.core.providers.base.location.LocationProvider;
import com.mardus.cycletrack.core.providers.base.speed.SpeedProvider;
import com.mardus.cycletrack.core.providers.google.location.GoogleServicesLocationProvider;
import com.mardus.cycletrack.core.providers.google.speed.GoogleServicesSpeedProvider;

public class GoogleServicesFactory implements ProvidersFactory {
    private GoogleApiClient googleApiClient;

    public GoogleServicesFactory(GoogleApiClient googleApiClient) {
        this.googleApiClient = googleApiClient;
    }

    @Override
    public LocationProvider getLocationProvider() {
        return new GoogleServicesLocationProvider(googleApiClient);
    }

    @Override
    public SpeedProvider getSpeedProvider() {
        return new GoogleServicesSpeedProvider(googleApiClient);
    }
}
