package com.mardus.cycletrack.core.providers.base;

import com.mardus.cycletrack.core.providers.base.misc.ServiceUnavailableException;

import java.util.ArrayList;
import java.util.List;

public abstract class SimpleProvider<T> implements BaseProvider<T> {

    protected List<ProviderDataListener<T>> providerDataListeners;
    protected ProviderConnectionListener providerConnectionListener;

    public SimpleProvider() {
        providerDataListeners = new ArrayList<>();
    }

    @Override
    public void addDataListener(ProviderDataListener<T> providerDataListener) {
        providerDataListeners.add(providerDataListener);
    }

    @Override
    public void removeDataListener(ProviderDataListener<T> providerDataListener) {
        providerDataListeners.remove(providerDataListener);
    }

    @Override
    public void connect(ProviderConnectionListener providerConnectionListener) throws ServiceUnavailableException {
        this.providerConnectionListener = providerConnectionListener;
    }

    protected void removeAllLocationListeners() {
        providerDataListeners.clear();
    }

    protected void sendDataToListeners(final T data) {
        for (ProviderDataListener<T> providerDataListener : providerDataListeners) {
            providerDataListener.onDataChanged(new SimpleProviderData<>(data));
        }
    }

    private static class SimpleProviderData<T> implements ProviderData<T> {
        private T value;

        SimpleProviderData(T value) {
            this.value = value;
        }

        @Override
        public T getValue() {
            return value;
        }
    }
}
