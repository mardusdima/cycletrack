package com.mardus.cycletrack.core.providers.google;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.mardus.cycletrack.core.providers.base.ProviderConnectionListener;
import com.mardus.cycletrack.core.providers.base.SimpleProvider;
import com.mardus.cycletrack.core.providers.base.misc.ServiceUnavailableException;

public abstract class GoogleServicesProvider<T> extends SimpleProvider<T> implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    protected GoogleApiClient googleApiClient;


    public GoogleServicesProvider(GoogleApiClient googleApiClient) {
        this.googleApiClient = googleApiClient;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        providerConnectionListener.onProviderConnected();
    }

    @Override
    public void onConnectionSuspended(int i) {
        //do nothing
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        providerConnectionListener.onProviderConnectionError();
    }

    @Override
    public boolean isConnected() {
        return googleApiClient.isConnected();
    }

    @Override
    public void connect(ProviderConnectionListener providerConnectionListener) throws ServiceUnavailableException {
        super.connect(providerConnectionListener);
        if (isConnected()) {
            providerConnectionListener.onProviderConnected();
            return;
        }

        googleApiClient.connect();
        googleApiClient.registerConnectionCallbacks(this);
        googleApiClient.registerConnectionFailedListener(this);
    }
}
