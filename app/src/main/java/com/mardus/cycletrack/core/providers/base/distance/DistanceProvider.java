package com.mardus.cycletrack.core.providers.base.distance;

import com.mardus.cycletrack.core.providers.base.BaseProvider;

public interface DistanceProvider extends BaseProvider<Integer> {
}
