package com.mardus.cycletrack.core.providers.base;

import com.mardus.cycletrack.core.providers.base.location.LocationProvider;
import com.mardus.cycletrack.core.providers.base.speed.SpeedProvider;

public interface ProvidersFactory {

    LocationProvider getLocationProvider();

    SpeedProvider getSpeedProvider();
}
