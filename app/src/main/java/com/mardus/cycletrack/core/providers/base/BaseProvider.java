package com.mardus.cycletrack.core.providers.base;

import com.mardus.cycletrack.core.providers.base.misc.ServiceNotConnectedException;
import com.mardus.cycletrack.core.providers.base.misc.ServiceUnavailableException;

public interface BaseProvider <T> {

    boolean isAvailable();

    boolean isConnected() throws ServiceUnavailableException;

    void connect(ProviderConnectionListener providerConnectionListener) throws ServiceUnavailableException;

    void startTracking() throws ServiceUnavailableException, ServiceNotConnectedException;

    void stopTracking();

    void addDataListener(ProviderDataListener<T> providerDataListener);

    void removeDataListener(ProviderDataListener<T> providerDataListener);
}
