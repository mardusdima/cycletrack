package com.mardus.cycletrack.core.providers.google.speed;

import android.location.Location;

import com.google.android.gms.common.api.GoogleApiClient;
import com.mardus.cycletrack.core.providers.base.speed.SpeedProvider;
import com.mardus.cycletrack.core.providers.google.GoogleServicesLocationBasedProvider;

public class GoogleServicesSpeedProvider extends GoogleServicesLocationBasedProvider<Float> implements SpeedProvider {

    public GoogleServicesSpeedProvider(GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    @Override
    protected Float extractDataFromLocation(Location location) {
        return location.getSpeed();
    }
}
