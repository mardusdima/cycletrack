package com.mardus.cycletrack.core.providers.mock.speed;

import com.mardus.cycletrack.core.providers.base.speed.SpeedProvider;
import com.mardus.cycletrack.core.providers.mock.SimpleMockedProvider;

public class SpeedMockedProvider extends SimpleMockedProvider<Float> implements SpeedProvider {
    private static final float INITIAL_SPEED = 3.0f;

    @Override
    protected void sendMockedData() {
        sendDataToListeners((float) (INITIAL_SPEED + Math.random()));
    }
}
