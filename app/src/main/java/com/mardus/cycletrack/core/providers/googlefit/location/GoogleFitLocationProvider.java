package com.mardus.cycletrack.core.providers.googlefit.location;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.Value;
import com.google.android.gms.fitness.request.DataSourcesRequest;
import com.google.android.gms.fitness.result.DataSourcesResult;
import com.mardus.cycletrack.core.providers.base.location.LocationProvider;
import com.mardus.cycletrack.core.providers.googlefit.GoogleFitProvider;
import com.mardus.cycletrack.model.GeoPoint;

public class GoogleFitLocationProvider extends GoogleFitProvider<GeoPoint> implements LocationProvider {

    public GoogleFitLocationProvider(GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    @Override
    protected DataSourcesRequest getDataSources() {
        return new DataSourcesRequest.Builder()
                .setDataTypes(DataType.TYPE_LOCATION_SAMPLE)
                .setDataSourceTypes(DataSource.TYPE_RAW)
                .build();
    }

    @Override
    protected void processDataSourcesResult(DataSourcesResult dataSourcesResult) {
        for (DataSource dataSource : dataSourcesResult.getDataSources()) {
            if (dataSource.getDataType().equals(DataType.TYPE_LOCATION_SAMPLE)) {
                registerFitnessDataListener(dataSource, DataType.TYPE_LOCATION_SAMPLE);
            }
        }
    }

    @Override
    public void onDataPoint(DataPoint dataPoint) {
        float latitude = 0;
        float longitude = 0;
        for (Field field : dataPoint.getDataType().getFields()) {
            Value value = dataPoint.getValue(field);
            if (field.equals(Field.FIELD_LATITUDE)) {
                latitude = value.asFloat();
            } else if (field.equals(Field.FIELD_LONGITUDE)) {
                longitude = value.asFloat();
            }
        }
        sendDataToListeners(new GeoPoint(latitude, longitude));
    }
}
