package com.mardus.cycletrack.core.services;

import com.mardus.cycletrack.core.providers.base.ProviderConnectionListener;
import com.mardus.cycletrack.core.providers.base.ProviderData;
import com.mardus.cycletrack.core.providers.base.ProviderDataListener;
import com.mardus.cycletrack.core.providers.base.location.LocationProvider;
import com.mardus.cycletrack.core.providers.base.misc.ServiceNotConnectedException;
import com.mardus.cycletrack.core.providers.base.misc.ServiceUnavailableException;
import com.mardus.cycletrack.core.providers.base.speed.SpeedProvider;
import com.mardus.cycletrack.core.services.base.RouteCalculator;
import com.mardus.cycletrack.core.services.base.TripListener;
import com.mardus.cycletrack.core.services.base.TripService;
import com.mardus.cycletrack.model.GeoPoint;
import com.mardus.cycletrack.model.Route;

import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;

public class CyclingTripService implements TripService, ProviderConnectionListener {
    public static final int DELAY = 1000;

    private final LocationProvider locationProvider;
    private SpeedProvider speedProvider;

    private List<TripListener> tripListeners;
    private SpeedListener speedListener;
    private LocationListener locationListener;
    private RouteCalculator routeCalculator;

    private Timer timer;
    private boolean isStarted;

    private Long time; //seconds
    private Float speed; //meters per second
    private Integer distance; //meters
    private Route route;

    public CyclingTripService(SpeedProvider speedProvider, LocationProvider locationProvider, RouteCalculator routeCalculator) {
        this.speedProvider = speedProvider;
        this.locationProvider = locationProvider;
        this.tripListeners = new CopyOnWriteArrayList<>();
        this.routeCalculator = routeCalculator;

        this.speedListener = new SpeedListener();
        this.locationListener = new LocationListener();

        resetValues();
    }

    @Override
    public void addTripListener(TripListener tripListener) {
        this.tripListeners.add(tripListener);
    }

    @Override
    public void removeTripListener(TripListener tripListener) {
        this.tripListeners.remove(tripListener);
    }

    @Override
    public boolean isTripStarted() {
        return isStarted;
    }

    @Override
    public void startTrip() throws ServiceNotConnectedException, ServiceUnavailableException {
        if(!locationProvider.isConnected()) {
            locationProvider.connect(this);
            return;
        }

        if (isStarted) {
            return;
        }
        isStarted = true;

        route = new Route();

        speedProvider.addDataListener(speedListener);
        speedProvider.startTracking();

        locationProvider.addDataListener(locationListener);
        locationProvider.startTracking();

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                time++;
                synchronized (CyclingTripService.this) {
                    distance = routeCalculator.calculateDistance(route);
                }
                sendDataToListeners();
            }
        }, DELAY, DELAY);

        notifyStartToListeners();
    }

    @Override
    public void stopTrip() {
        if (!isStarted) {
            return;
        }

        isStarted = false;
        speedProvider.removeDataListener(speedListener);
        speedProvider.stopTracking();

        locationProvider.removeDataListener(locationListener);

        if (timer != null) {
            timer.cancel();
        }

        resetValues();

        notifyStopToListeners();
    }

    @Override
    public Route getRoute() {
        return route;
    }

    @Override
    public void onProviderConnected() {
        //no need
    }

    @Override
    public void onProviderConnectionError() {
        //no need
    }

    private void sendDataToListeners() {
        for (TripListener tripListener : tripListeners) {
            tripListener.onTripInfoChanged(speed, distance, time);
        }
    }

    private void sendLocationToListeners(GeoPoint geoPoint) {
        for (TripListener tripListener : tripListeners) {
            tripListener.onLocationChanged(geoPoint);
        }
    }

    private void notifyStartToListeners() {
        for (TripListener tripListener : tripListeners) {
            tripListener.onTripStarted();
        }
    }

    private void notifyStopToListeners() {
        for (TripListener tripListener : tripListeners) {
            tripListener.onTripStopped();
        }
    }

    private void resetValues() {
        time = 0L;
        distance = 0;
        speed = 0F;
    }

    private class SpeedListener implements ProviderDataListener<Float> {
        @Override
        public void onDataChanged(ProviderData<Float> data) {
            speed = data.getValue();
        }
    }

    private class LocationListener implements ProviderDataListener<GeoPoint> {
        @Override
        public void onDataChanged(ProviderData<GeoPoint> data) {
            synchronized (CyclingTripService.this) {
                GeoPoint value = data.getValue();
                route.addGeoPoint(new Date(), value);
                sendLocationToListeners(value);
            }
        }
    }
}
