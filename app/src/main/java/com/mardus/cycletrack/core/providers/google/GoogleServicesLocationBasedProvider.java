package com.mardus.cycletrack.core.providers.google;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.mardus.cycletrack.App;
import com.mardus.cycletrack.core.providers.base.misc.ServiceNotConnectedException;
import com.mardus.cycletrack.core.providers.base.misc.ServiceUnavailableException;
import com.mardus.cycletrack.utils.Utils;

public abstract class GoogleServicesLocationBasedProvider<T> extends GoogleServicesProvider<T> implements LocationListener {

    private static final long UPDATE_INTERVAL = 5000;
    private static final long FASTEST_INTERVAL = 1000;
    private boolean isTrackingLocation;

    public GoogleServicesLocationBasedProvider(GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    @Override
    public boolean isAvailable() {
        boolean hasPermissionsForLocation = Utils.isLocationPermissionsEnabled(App.getInstance());
        return googleApiClient != null && hasPermissionsForLocation;
    }

    @Override
    public void startTracking() throws ServiceUnavailableException, ServiceNotConnectedException {
        if (!googleApiClient.isConnected()) {
            throw new ServiceNotConnectedException();
        }
        getLastKnownLocation();
        if (isTrackingLocation) {
            return;
        }
        requestLocationCallbacks();
    }

    @Override
    public void stopTracking() {
        isTrackingLocation = false;
        if (googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            googleApiClient.unregisterConnectionCallbacks(this);
        }
        removeAllLocationListeners();
    }

    @Override
    public void onLocationChanged(final Location location) {
        sendDataToListeners(extractDataFromLocation(location));
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        super.onConnected(bundle);
        try {
            getLastKnownLocation();
            requestLocationCallbacks();
        } catch (ServiceUnavailableException e) {
            providerConnectionListener.onProviderConnectionError();
        }
    }

    abstract protected T extractDataFromLocation(Location location);

    private void getLastKnownLocation() throws ServiceUnavailableException {
        Location lastLocation;
        try {
            lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        } catch (SecurityException e) {
            throw new ServiceUnavailableException();
        }
        if (lastLocation == null) {
            return;
        }
        sendDataToListeners(extractDataFromLocation(lastLocation));

    }

    private void requestLocationCallbacks() throws ServiceUnavailableException {
        isTrackingLocation = true;
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);
        try {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        } catch (SecurityException e) {
            throw new ServiceUnavailableException();
        }
    }
}

