package com.mardus.cycletrack.model;

public class GeoPoint {

    public final double latitude;
    public final double longitude;

    public GeoPoint(double lon, double lat) {
        if(-180.0D <= lat && lat < 180.0D) {
            this.longitude = lat;
        } else {
            this.longitude = ((lat - 180.0D) % 360.0D + 360.0D) % 360.0D - 180.0D;
        }

        this.latitude = Math.max(-90.0D, Math.min(90.0D, lon));
    }

    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        } else if(!(obj instanceof GeoPoint)) {
            return false;
        } else {
            GeoPoint var2 = (GeoPoint)obj;
            return Double.doubleToLongBits(this.latitude) == Double.doubleToLongBits(var2.latitude) && Double.doubleToLongBits(this.longitude) == Double.doubleToLongBits(var2.longitude);
        }
    }

    public String toString() {
        double var1 = this.latitude;
        double var3 = this.longitude;
        return (new StringBuilder(60)).append("lat/lng: (").append(var1).append(",").append(var3).append(")").toString();
    }
}
