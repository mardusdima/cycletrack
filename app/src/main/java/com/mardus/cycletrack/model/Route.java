package com.mardus.cycletrack.model;

import android.support.v4.util.Pair;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Route implements Cloneable {

    private List<Pair<Date, GeoPoint>> geoPointList;

    public Route() {
        geoPointList = new ArrayList<>();
    }

    public List<Pair<Date, GeoPoint>> getGeoPointList() {
        return new ArrayList<>(geoPointList);
    }

    public void addGeoPoint(Date time, GeoPoint geoPoint) {
        geoPointList.add(new Pair<>(time, geoPoint));
    }
}
