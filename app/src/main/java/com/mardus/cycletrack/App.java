package com.mardus.cycletrack;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.location.LocationServices;
import com.mardus.cycletrack.core.CyclingManager;
import com.mardus.cycletrack.core.providers.base.ProvidersFactory;
import com.mardus.cycletrack.core.providers.google.GoogleServicesFactory;
import com.mardus.cycletrack.core.providers.googlefit.GoogleFitProvidersFactory;
import com.mardus.cycletrack.core.providers.mock.MockedServicesFactory;
import com.mardus.cycletrack.core.services.AndroidRouteCalculator;

public class App extends Application {
    private static App instance;

    private GoogleApiClient googleApiClient;
    private CyclingManager cyclingManager;

    public static App getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (instance == null) {
            instance = this;
        }

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public CyclingManager getCyclingManager() {
        return cyclingManager;
    }

    public void initCyclingManager(FragmentActivity activity) {
        googleApiClient = buildGoogleApiClient(activity);
        ProvidersFactory fitnessProvidersFactory = new GoogleFitProvidersFactory(googleApiClient);
        ProvidersFactory googleProvidersFactory = new GoogleServicesFactory(googleApiClient);
        ProvidersFactory mockedProvidersFactory = new MockedServicesFactory();
        AndroidRouteCalculator routeDistanceCalculator = new AndroidRouteCalculator();

        cyclingManager = new CyclingManager(googleProvidersFactory, fitnessProvidersFactory, mockedProvidersFactory, routeDistanceCalculator);
        cyclingManager.initGoogleProviders();
    }

    private GoogleApiClient buildGoogleApiClient(FragmentActivity activity) {
        return googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Fitness.SENSORS_API)
                .addApi(LocationServices.API)
                .addScope(new Scope(Scopes.FITNESS_LOCATION_READ))
                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ))
                .addScope(new Scope(Scopes.FITNESS_BODY_READ))
                .enableAutoManage(activity, 0, null)
                .build();
    }

}
