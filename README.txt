CycleTrack is simple application for cycling.

UI

Application has one Activity (CyclingTripActivity) and one Service (CyclingTripService).

Activity contains TabBar with two tabs and NavigationDrawer with settings.
Tabs:
1 - Trip Info Tab : time, speed, distance and ect. (CyclingInfoFragment)
2 - Trip Map Tab : show your location. (CyclingMapFragment)
NavigationDrawer:
Has two option for using sensors:
1 - use real sensors (Google Api)
2 - use mocked sensors (mocked speed, location)
CyclingTripActivity supports screen rotations.

Service runs foreground and shows user rote distance.

ARCHITECTURE

Data providers:
BaseProvider - interface for any real-time sensor-data providers (LocationProvider, SpeedProvider)
ProviderConnectionListener - interface for provider connection listener
ProviderDataListener - interface for data listening from BaseProvider

Logic components:
TripService - interface that should aggregate data from any data providers
TripListener - interface for data listening from TripService

Main implementations:
GoogleServicesLocationProvider, GoogleServicesSpeedProvider, LocationMockedProvider, SpeedMockedProvider
CyclingAppManager - class that handle switching between real and mocked sensors, uses ProvidersFactory.

TESTS
Added one simple test (CyclingTripServiceTest) to show that code can be covered with Unit tests


